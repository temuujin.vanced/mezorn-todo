export function GetNow(fixedMinutes) {
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1;
  var yyyy = today.getFullYear();
  var hh = today.getHours();
  var mins = today.getMinutes();

  if (dd < 10) {
    dd = "0" + dd;
  }
  if (mm < 10) {
    mm = "0" + mm;
  }
  if (mins < 10) {
    mins = "0" + mins;
  }

  if (fixedMinutes) {
    if (mins < 40) {
      mins = parseInt(mins) + 10;
    } else {
      hh = parseInt(hh) + 1;
    }
  }

  if (hh < 10) {
    hh = "0" + hh;
  }
  return (today = yyyy + "-" + mm + "-" + dd + "T" + hh + ":" + mins);
}

export function millisToDays(millis) {
  return (millis / (1000 * 3600 * 24)).toFixed(0);
}

export function millisToHours(millis) {
  return (millis / (1000 * 3600)).toFixed(0);
}

export function millisToMinutesAndSeconds(millis) {
  var minutes = Math.floor(millis / 60000);
  var seconds = ((millis % 60000) / 1000).toFixed(0);
  return minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
}

export function returnRemainingTime(millis) {
  if (millis < 0) {
    return "Overdue";
  }
  const days = millisToDays(millis);
  const hours = millisToHours(millis);

  if (days < 1) {
    if (hours < 1) {
      return millisToMinutesAndSeconds(millis);
    }
    return hours + (hours == 1 ? " hour" : " hours");
  }
  return days + (days == 1 ? " day" : " days");
}

export function SetAppStorage(tasks){
  window.localStorage.setItem("appState", JSON.stringify(tasks));
}