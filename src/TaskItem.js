import React, { useState, useEffect } from "react";

import DeleteForeverIcon from "mdi-react/DeleteForeverIcon";
import EditIcon from "mdi-react/EditIcon";
import DoneIcon from "mdi-react/DoneIcon";
import ContentSaveIcon from "mdi-react/ContentSaveIcon";
import { GetNow, returnRemainingTime, millisToHours } from "./func.js";

const TaskItem = ({ todo, handleToggle, handleDelete, handleUpdate }) => {
  const [userInput, setUserInput] = useState(todo.task);
  const [taskDue, setTaskDue] = useState(todo.due);
  const [canEdit, setCanEdit] = useState(false);
  const [timerState, setTimerState] = useState(getTimeDifference());
  let tickInterval = "";

  function getTimeDifference() {
    if (taskDue == "") {
      return new Date();
    }
    return new Date(todo.due) - new Date();
  }
  const handleClick = (e, type) => {
    e.preventDefault();

    switch (type) {
      case "toggle":
        handleToggle(e.currentTarget.id);
        break;
      case "delete":
        handleDelete(e.currentTarget.id);
        break;
    }
  };
  const handleTaskChange = (e) => {
    setUserInput(e.currentTarget.value);
  };
  const handleDueChange = (e) => {
    setTaskDue(e.currentTarget.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    handleUpdate(todo.id, userInput, taskDue);
    setCanEdit(false);
  };

  useEffect(() => {
    if (millisToHours(getTimeDifference()) < 1 && getTimeDifference() > 0) {
      tickInterval = setInterval(() => {
        if (getTimeDifference() < 0) {
          clearInterval(tickInterval);
          tickInterval = "";
        }
        setTimerState(getTimeDifference());
      }, 1000);

      [("pagehide", "beforeunload")].forEach(function (e) {
        window.addEventListener(e, () => {
          clearInterval(tickInterval);
          tickInterval = "";
        });
      });
    }
  }, [taskDue]);

  return (
    <form
      onSubmit={handleSubmit}
      className={`flex flex-col sm:flex-row items-center justify-center sm:justify-between bg-white rounded-md p-3 space-y-10 sm:space-y-0 sm:space-x-10 ${
        todo.complete ? "bg-opacity-40" : "bg-opacity-100"
      }`}
    >
      <div className="flex flex-col sm:flex-row justify-center items-center sm:space-x-10">
        <input
          type="text"
          value={userInput}
          disabled={!canEdit}
          className="disabled:bg-transparent bg-gray-100 rounded-md p-2 text-center sm:text-left"
          onChange={handleTaskChange}
        />
        {canEdit ? (
          <input
            type="datetime-local"
            value={taskDue}
            disabled={!canEdit}
            min={GetNow()}
            className={`disabled:bg-transparent bg-gray-100 rounded-md p-2 ${
              taskDue === "" && !canEdit ? "opacity-0" : "opacity-100"
            }`}
            onChange={handleDueChange}
          />
        ) : (
          <div>
            {taskDue === "" || todo.complete
              ? ""
              : tickInterval == ""
              ? returnRemainingTime(getTimeDifference())
              : returnRemainingTime(timerState)}
          </div>
        )}
      </div>
      {canEdit && !todo.complete ? (
        <button
          type="submit"
          className="cursor-pointer p-2 pr-3 rounded-md bg-orange-400 flex items-center space-x-2 text-sm font-bold text-white"
        >
          <ContentSaveIcon className="fill-current " />
          <p>Save</p>
        </button>
      ) : (
        <div className="flex flex-row space-x-3 text-white overflow-hidden">
          {!todo.complete ? (
            <button
              id={todo.id}
              onClick={(e) => {
                handleClick(e, "toggle");
              }}
              className="cursor-pointer p-2 pr-3 rounded-md bg-green-400 flex items-center space-x-2 text-sm font-bold "
            >
              <DoneIcon className="fill-current " />
              <p>Complete</p>
            </button>
          ) : (
            <div className="text-green-400 py-2 px-3">Completed</div>
          )}
          {!todo.complete && (
            <button
              onClick={() => {
                setCanEdit(true);
              }}
              className="cursor-pointer p-2 rounded-md bg-blue-400"
            >
              <EditIcon className="fill-current " />
            </button>
          )}
          <button
            id={todo.id}
            onClick={(e) => {
              handleClick(e, "delete");
            }}
            className="cursor-pointer p-2 rounded-md bg-red-400"
          >
            <DeleteForeverIcon className="fill-current " />
          </button>
        </div>
      )}
    </form>
  );
};

export default TaskItem;
