import React, { useState } from "react";

import { GetNow } from "./func.js";

const TaskForm = ({ addTask }) => {
  const [userInput, setUserInput] = useState("");
  const [taskDue, setTaskDue] = useState("");

  const handleTaskChange = (e) => {
    setUserInput(e.currentTarget.value);
  };
  const handleDueChange = (e) => {
    setTaskDue(e.currentTarget.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    addTask(userInput, taskDue);
    setUserInput("");
    setTaskDue("");
  };

  return (
    <form
      onSubmit={handleSubmit}
      className="flex flex-col space-y-3 sm:space-y-0 sm:flex-row sm:space-x-5"
    >
      <input
        type="text"
        value={userInput}
        onChange={handleTaskChange}
        placeholder="Write task here..."
        className="rounded-md px-6 py-3"
        required
      />
      <input
        type="datetime-local"
        id="enddate"
        name="enddate"
        // value={"2014-01-02T11:42"}
        value={taskDue}
        min={GetNow()}
        onChange={handleDueChange}
        className="rounded-md px-6 py-3"
      ></input>
      <button
        type="submit"
        className="bg-orange-400 text-white font-bold px-6 py-3 rounded-md"
      >
        Add
      </button>
    </form>
  );
};

export default TaskForm;
