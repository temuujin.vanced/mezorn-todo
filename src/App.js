import React, { useEffect, useState } from "react";
import { GetNow, SetAppStorage } from "./func";

import TaskForm from "./TaskForm";
import TaskList from "./TaskList";

function App() {
  const [taskList, setTaskList] = useState([
    {
      id: 1,
      task: "Do chores",
      due: GetNow(true),
      complete: false,
    },
    {
      id: 2,
      task: "Cook dinner",
      due: "2021-08-02T11:10",
      complete: false,
    },
    {
      id: 3,
      task: "Homework",
      due: "",
      complete: false,
    },
    {
      id: 4,
      task: "Date night",
      due: "2022-08-02T11:42",
      complete: true,
    },
  ]);

  const handleToggle = (id) => {
    let changed;
    taskList.map((task) => {
      if (task.id === Number(id)) {
        changed = { ...task, complete: true };
      }
    });
    let filtered = taskList.filter((task) => task.id !== Number(id));
    filtered.push(changed);
    setTaskList(filtered);
    SetAppStorage(filtered);
  };

  const handleDelete = (id) => {
    let filtered = taskList.filter((task) => task.id !== Number(id));
    setTaskList(filtered);
    SetAppStorage(filtered);
  };

  const addTask = (userInput, taskDue) => {
    let copy = [...taskList];

    copy.unshift({
      id: taskList.length > 0 ? taskList[taskList.length - 1].id++ : 1,
      task: userInput,
      due: taskDue,
      complete: false,
    });
    setTaskList(copy);
    SetAppStorage(copy);
  };

  const updateTask = (id, userInput, taskDue) => {
    let mapped = taskList.map((task) => {
      return task.id === Number(id)
        ? { ...task, task: userInput, due: taskDue, complete: false }
        : { ...task };
    });
    setTaskList(mapped);
    SetAppStorage(mapped);
  };

  useEffect(() => {
    if (window.localStorage.getItem("appState")) {
      setTaskList(JSON.parse(window.localStorage.getItem("appState")));
    } else {
      SetAppStorage(taskList);
    }
  }, []);

  return (
    <div className="w-screen p-10 flex flex-col items-center justify-start">
      <TaskForm addTask={addTask} />
      <TaskList
        taskList={taskList}
        handleToggle={handleToggle}
        handleDelete={handleDelete}
        handleUpdate={updateTask}
      />
    </div>
  );
}

export default App;
