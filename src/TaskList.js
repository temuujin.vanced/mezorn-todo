import React from "react";
import TaskItem from "./TaskItem";

const TaskList = ({ taskList, handleToggle, handleDelete, handleUpdate }) => {
  return (
    <div className="flex flex-col mt-5 space-y-3">
      {taskList.map((todo) => {
        return (
          <TaskItem
            todo={todo}
            key={todo.id + todo.task}
            handleToggle={handleToggle}
            handleDelete={handleDelete}
            handleUpdate={handleUpdate}
          />
        );
      })}
    </div>
  );
};

export default TaskList;
